<?php

namespace App\Http\Controllers\Backend;

use App\Models\District;
use App\Models\Division;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DivisionController extends Controller{
    //
    public function __construct(){
        $this->middleware('auth:admin');
    }

    public function index(){
        $division = Division::orderBy('priority','asc')->get();
        return view('backend.pages.division.manage_division')->with('divisions',$division);
    }

    public function create(){
        return view('backend.pages.division.create');
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required|max:255',
            'priority' => 'required',
        ]);

        $division = new Division();
        $division->name = $request->name;
        $division->priority = $request->priority;

        $division->save();

        session()->flash('success','A New Division Has Added Successfully');
        return redirect()->route('admin.manage_division');
    }

    public function edit($id){

        $division = Division::find($id);
        if (!is_null($division)) {
            return view('backend.pages.division.edit',compact('division'));
        }else{
            return redirect()->route('admin.manage_division');
        }
    }

    public function update(Request $request,$id){
        $request->validate([
            'name' => 'required|max:255',
            'priority' => 'required',
        ]);

        $division = Division::find($id);
        $division->name = $request->name;
        $division->priority = $request->priority;

        $division->save();

        session()->flash('success',' Division Has Updated Successfully');
        return redirect()->route('admin.manage_division');
    }

    public function delete($id){
        $division = Division::find($id);

        if (!is_null($division)){

//          delete all the districts for this divisions
            $districts = District::where('division_id' == $division->id);
            foreach ($districts as $district){
                $district->delete();
            }

            $division->delete();
        }

        session()->flash('success','Division Has Deleted Successfully');

        return redirect()->route('admin.manage_division');
    }
}
