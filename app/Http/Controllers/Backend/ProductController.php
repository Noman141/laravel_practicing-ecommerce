<?php

namespace App\Http\Controllers\Backend;

use Intervention\Image\Facades\Image as Image;
use App\Models\Product;
use App\Models\ProductImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;

class ProductController extends Controller{

    public function __construct(){
        $this->middleware('auth:admin');
    }
    public function index(){
        $product = Product::orderBy('id','desc')->get();
        return view('backend.pages.products.manage-product')->with('products',$product);
    }

    public function create(){
        return view('backend.pages.products.create');
    }

    public function edit($id){
        $product = Product::find($id);
        return view('backend.pages.products.edit-product')->with('product',$product);
    }

    public function store(Request $request){

        $request->validate([
            'title'       => 'required|max:255',
            'description' => 'required',
            'quantity' => 'required|numeric',
            'price' => 'required|numeric',
            'category_id' => 'required|numeric',
            'brand_id' => 'required|numeric',
        ]);


        $product = new Product();
        $product->title = $request->title;
        $product->description = $request->description;
        $product->quantity = $request->quantity;
        $product->price = $request->price;
        $product->slug = str_slug($request->title);

        $product->category_id = $request->category_id;
        $product->brand_id = $request->brand_id;
        $product->admin_id = 1;

        $product->save();

        /*
                 * for upload single image
                if ($request->hasFile('image')){
                    $image = $request->file('image');
                    $img_name = time().'.'.$image->getClientOriginalExtension();
                    $location = public_path('img/products/'.$img_name);
                    Image::make($image)->save($location);

                    $product_image = new ProductImage();
                    $product_image->product_id = $product->id;
                    $product_image->image = $img_name;
                    $product_image->save();
                }
        */

//upload multiple image

        if (count($request->image) > 0) {
            foreach ($request->image as $image) {
                //insert that image
                //$image = $request->file('product_image');
                $img = time() . '.'. $image->getClientOriginalExtension();
                $location = public_path('img/products/' .$img);
                Image::make($image)->save($location);
                $product_image = new ProductImage;
                $product_image->product_id = $product->id;
                $product_image->image = $img;
                $product_image->save();
            }
        }


        return redirect()->route('admin.manage_product');
    }

    public function update(Request $request,$id){
        $request->validate([
            'title'       => 'required|max:255',
            'description' => 'required',
            'quantity' => 'required|numeric',
            'price' => 'required|numeric',
            'category_id' => 'required|numeric',
            'brand_id' => 'required|numeric',
        ]);

        $product =Product::find($id);
        $product->title = $request->title;
        $product->description = $request->description;
        $product->quantity = $request->quantity;
        $product->price = $request->price;
        $product->slug = str_slug($request->title);

        $product->category_id = $request->category_id;
        $product->brand_id = $request->brand_id;
        $product->admin_id = 1;

//        delete old image


        if (count($request->image) > 0) {
            foreach ($request->image as $image) {
                //insert that image
                //$image = $request->file('product_image');
                $img = time() . '.'. $image->getClientOriginalExtension();
                $location = public_path('img/products/' .$img);
                Image::make($image)->save($location);
                $product_image = new ProductImage;
                $product_image->product_id = $product->id;
                $product_image->image = $img;
                $product_image->save();
            }
        }

        $product->save();

        return redirect()->route('admin.manage_product');
    }

    public function delete($id){
        $product = Product::find($id);

        if (!is_null($product)){
//
//          Delete Category Image
            if (File::exists('img/brands/'.$product->image)){
                File::delete('img/brands/'.$product->image);
            }
            $product->delete();
        }

       session()->flash('success','Product Has Deleted Successfully');

       return redirect()->route('admin.manage_product');
    }
}
