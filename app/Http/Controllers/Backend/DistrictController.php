<?php

namespace App\Http\Controllers\Backend;

use App\Models\District;
use App\Models\Division;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DistrictController extends Controller{
    //
    public function __construct(){
        $this->middleware('auth:admin');
    }

    public function index(){
        $district = District::orderBy('name','asc')->get();
        return view('backend.pages.district.manage_district')->with('districts',$district);
    }

    public function create(){
        $divisions = Division::orderBy('priority','asc')->get();
        return view('backend.pages.district.create',compact('divisions'));
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required|max:255',
            'division_id' => 'required',
        ]);

        $district = new District();
        $district->name = $request->name;
        $district->division_id = $request->division_id;

        $district->save();

        session()->flash('success','A New Division Has Added Successfully');
        return redirect()->route('admin.manage_district');
    }

    public function edit($id){
        $divisions = Division::orderBy('priority','asc')->get();
        $districts = District::find($id);
        if (!is_null($districts)) {
            return view('backend.pages.district.edit',compact('districts','divisions'));
        }else{
            return redirect()->route('admin.manage_district');
        }
    }

    public function update(Request $request,$id){
        $request->validate([
            'name' => 'required|max:255',
            'division_id' => 'required',
        ]);

        $district = District::find($id);
        $district->name = $request->name;
        $district->division_id = $request->division_id;

        $district->save();

        session()->flash('success',' District Has Updated Successfully');
        return redirect()->route('admin.manage_district');
    }

    public function delete($id){
        $district = District::find($id);

        if (!is_null($district)){

            $district->delete();
        }

        session()->flash('success','District Has Deleted Successfully');

        return redirect()->route('admin.manage_district');
    }
}
