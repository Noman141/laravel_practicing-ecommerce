<?php

namespace App\Http\Controllers\Backend;

use Intervention\Image\Facades\Image as Image;
use File;
use App\Models\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SlidersController extends Controller{

    public function __construct(){
        $this->middleware('auth:admin');
    }

    public function index(){
        $sliders = Slider::orderBy('priority','asc')->get();
        return view('backend.pages.sliders.manage_slider',compact('sliders'));
    }

    public function store(Request $request){
        $this->validate($request,[
            'title'   => 'required',
            'image'   =>'required|image',
            'priority'=>'required',
            'button_link' => 'nullable|url'
        ]);

        $slider = new Slider();

        $slider->title = $request->title;
        $slider->button_text = $request->button_text;
        $slider->button_link = $request->button_link;
        $slider->priority = $request->priority;

        if ($request->image > 0){
            $image = $request->file('image');
            $img_name = time().'.'.$image->getClientOriginalExtension();
            $location = public_path('img/sliders/'.$img_name);
            Image::make($image)->save($location);
            $slider->image = $img_name;
        }

        $slider->save();

        session()->flash('success','A new Slider has been added successfully!');
        return redirect()->route('admin.manage_slider');
    }

    public function update(Request $request,$id){
        $this->validate($request,[
            'title'   => 'required',
            'image'   =>'nullable|image',
            'priority'=>'required',
            'button_link' => 'nullable|url'
        ]);

        $slider = Slider::find($id);

        $slider->title = $request->title;
        $slider->button_text = $request->button_text;
        $slider->button_link = $request->button_link;
        $slider->priority = $request->priority;

//        delete old slider

        if ($request->image >0 ){
            if (File::exists('img/sliders/'.$slider->image)){
                File::delete('img/sliders/'.$slider->image);
            }

            $image = $request->file('image');
            $img_name = time().'.'.$image->getClientOriginalExtension();
            $location = public_path('img/sliders/'.$img_name);
            Image::make($image)->save($location);
            $slider->image = $img_name;
        }
        $slider->save();

        session()->flash('success',' Slider has been Updated successfully!');
        return redirect()->route('admin.manage_slider');
    }

    public function delete($id){
        $slider = Slider::find($id);

        if (!is_null($slider)){
//
//          Delete Category Image
            if (File::exists('img/sliders/'.$slider->image)){
                File::delete('img/sliders/'.$slider->image);
            }
            $slider->delete();
        }

        session()->flash('success','Slider Has Deleted Successfully');

        return redirect()->route('admin.manage_slider');
    }
}
