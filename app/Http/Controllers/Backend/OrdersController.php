<?php

namespace App\Http\Controllers\Backend;

use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrdersController extends Controller{
    //
    public function __construct(){
        $this->middleware('auth:admin');
    }

    public function index(){
        $orders = Order::orderBy('id','desc')->get();
        return view('backend.pages.order.manage_order')->with('orders',$orders);
    }

    public function show($id){
        $order = Order::find($id);
        $order->is_seen_by_admin = 1;
        $order->save();
        return view('backend.pages.order.show',compact('order'));
    }

    public function completed($id){
         $order = Order::find($id);
         if ($order->is_competed){
             $order->is_competed = 0;
         }else{
             $order->is_competed = 1;
         }
        $order->save();
         session()->flash('success','Order Completed Status has Changed');
         return back();
    }

    public function paid($id){
        $order = Order::find($id);
        if ($order->is_paid){
            $order->is_paid = 0;
        }else{
            $order->is_paid = 1;
        }
        $order->save();
        session()->flash('success','Order Paid Status has Changed');
        return back();
    }
}
