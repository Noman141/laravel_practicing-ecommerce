<?php

namespace App\Http\Controllers\Backend;

use App\Models\Brand;
use Intervention\Image\Facades\Image as Image;
use File;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class BrandController extends Controller{
    public function __construct(){
        $this->middleware('auth:admin');
    }

    public function index(){
        $brands = Brand::orderBy('name','asc')->get();
        return view('backend.pages.brand.manage_brand')->with('brands',$brands);
    }

    public function create(){
        $brands = Category::orderBy('name','asc')->get();
        return view('backend.pages.brand.create')->with('brands',$brands);
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required|max:255',
            'image' => 'nullable|image',
        ]);

        $category = new Brand();
        $category->name = $request->name;
        $category->description = $request->description;

//        insert image
        if ($request->hasFile('image')){
            $image = $request->file('image');
            $img_name = time().'.'.$image->getClientOriginalExtension();
            $location = public_path('img/brands/'.$img_name);
            Image::make($image)->save($location);
            $category->image = $img_name;
        }

        $category->save();

        session()->flash('success','A New Category Has Added Successfully');
        return redirect()->route('admin.manage_brand');
    }

    public function edit($id){

        $brand = Brand::find($id);
        if (!is_null($brand)) {
            return view('backend.pages.brand.edit',compact('brand'));
        }else{
            return redirect()->route('admin.manage_brand');
        }
    }

    public function update(Request $request,$id){
        $request->validate([
            'name' => 'required|max:255',
            'image' => 'nullable|image',
        ]);

        $brand = Brand::find($id);
        $brand->name = $request->name;
        $brand->description = $request->description;
//        delete old image
        if (File::exists('img/brands/'.$brand->image)){
            File::delete('img/brands/'.$brand->image);
        }
//        insert image
        if ($request->hasFile('image')){
            $image = $request->file('image');
            $img_name = time().'.'.$image->getClientOriginalExtension();
            $location = public_path('img/brands/'.$img_name);
            Image::make($image)->save($location);
            $brand->image = $img_name;
        }



        $brand->save();

        session()->flash('success',' Category Has Updated Successfully');
        return redirect()->route('admin.manage_brand');
    }

    public function delete($id){
        $brand = Brand::find($id);

        if (!is_null($brand)){
//
//          Delete Category Image
            if (File::exists('img/brands/'.$brand->image)){
                File::delete('img/brands/'.$brand->image);
            }
            $brand->delete();
        }

        session()->flash('success','Category Has Deleted Successfully');

        return redirect()->route('admin.manage_brand');
    }
}
