<?php

namespace App\Http\Controllers\Backend;

use Intervention\Image\Facades\Image as Image;
use App\Models\Product;
use App\Models\ProductImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PagesController extends Controller{
    public function __construct(){
        $this->middleware('auth:admin');
    }
    public function index(){
        return view('backend.pages.index');
    }
}
