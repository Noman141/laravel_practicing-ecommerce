<?php

namespace App\Http\Controllers\Backend;
use Intervention\Image\Facades\Image as Image;
use File;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller{
    public function __construct(){
        $this->middleware('auth:admin');
    }

    public function index(){
        $category = Category::orderBy('id','desc')->get();
        return view('backend.pages.category.manage_category')->with('categories',$category);
    }

    public function create(){

        $main_category = Category::orderBy('name','asc')->where('parent_id',NULL)->get();
        return view('backend.pages.category.create')->with('main_categories',$main_category);
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required|max:255',
            'image' => 'nullable|image',
        ]);

        $category = new Category();
        $category->name = $request->name;
        $category->description = $request->description;
//        $category->image = $request->image;
        $category->parent_id = $request->parent_id;

//        insert image
        if ($request->hasFile('image')){
            $image = $request->file('image');
            $img_name = time().'.'.$image->getClientOriginalExtension();
            $location = public_path('img/category/'.$img_name);
            Image::make($image)->save($location);
            $category->image = $img_name;
        }

        $category->save();

        session()->flash('success','A New Category Has Added Successfully');
        return redirect()->route('admin.manage_category');
    }

    public function edit($id){

        $main_category = Category::orderBy('name','asc')->where('parent_id',NULL)->get();
        $category = Category::find($id);
        if (!is_null($category)) {
            return view('backend.pages.category.edit',compact('category','main_category'));
        }else{
            return redirect()->route('admin.manage_category');
        }
    }

    public function update(Request $request,$id){
        $request->validate([
            'name' => 'required|max:255',
            'image' => 'nullable|image',
        ]);

        $category = Category::find($id);
        $category->name = $request->name;
        $category->description = $request->description;
        $category->parent_id = $request->parent_id;
//        delete old image
        if (File::exists('img/category/'.$category->image)){
            File::delete('img/category/'.$category->image);
        }
//        insert image
        if ($request->hasFile('image')){
            $image = $request->file('image');
            $img_name = time().'.'.$image->getClientOriginalExtension();
            $location = public_path('img/category/'.$img_name);
            Image::make($image)->save($location);
            $category->image = $img_name;
        }



        $category->save();

        session()->flash('success',' Category Has Updated Successfully');
        return redirect()->route('admin.manage_category');
    }

    public function delete($id){
        $category = Category::find($id);

        if (!is_null($category)){
//          If it is a parent category then delete all its sub category

            if ($category->parent_id == NULL){
                $sub_category = Category::orderBy('name','asc')->where('parent_id',$category->id)->get();
                foreach ($sub_category as $sub){
                    if (File::exists('img/category/'.$sub->image)){
                        File::delete('img/category/'.$sub->image);
                    }
                    $sub->delete();
                }
            }

//          Delete Category Image
            if (File::exists('img/category/'.$category->image)){
                File::delete('img/category/'.$category->image);
            }
            $category->delete();
        }

        session()->flash('success','Category Has Deleted Successfully');

        return redirect()->route('admin.manage_category');
    }
}
