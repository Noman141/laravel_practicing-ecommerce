<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Notifications\VerifyRegistration;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login( Request $request){
        $this->validate($request,[
            'email' => 'required|email',
            'password' => 'required',
        ]);

//        find user by this email
        $user = User::where('email',$request->email)->first();
        if (!isset($user)){

            session()->flash('errormsg','You are not registered. Please Register First');
            return redirect('login');

        }elseif ($user->status == 1){
//            login this user
            if (Auth::guard('web')->attempt(['email' => $request->email,'password' => $request->password],$request->remember)){
//            log him now
                return redirect()->intended(route('index'));
            }else{
                session()->flash('errormsg','Something wents wrong!!!Please try again');
                return redirect('login');

            }
        }else{
//         send him a token again
            if($user->status == 0){
                $user->notify(new VerifyRegistration($user));
                session()->flash('success','A new confirmation has sent to you.Please Confirm your Email.');
                return redirect('login');
            }
        }
    }
}
