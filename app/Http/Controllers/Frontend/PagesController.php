<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Product;
use App\Http\Controllers\Controller;
use App\Models\Slider;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    //load show.blade.php file
    public function index(){

        $products = Product::orderBy('id','desc')->paginate(9);
        $sliders = Slider::orderBy('priority','asc')->get();
        return view('frontend.pages.index',compact('products','sliders'));
    }

    public function contact(){
        return view('frontend.pages.contact');
    }

    public function products(){
        $products = Product::orderBy('id','desc')->get();
        return view('frontend.pages.product.index')->with('products',$products);
    }

    public function search(Request $request){
         $search = $request->search;

         if (!empty($search)){
             $products = Product::orwhere('title','like','%'.$search.'%')
                      ->orwhere('description','like','%'.$search.'%')
                      ->orwhere('slug','like','%'.$search.'%')
                      ->orwhere('price','like','%'.$search.'%')
                      ->orwhere('quantity','like','%'.$search.'%')
                      ->orderBy('id','desc')
                      ->paginate(9);
             return view('frontend.pages.product.search',compact('products','search'));
         }
    }
}
