<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Cart;
use App\Models\Order;
use App\Models\Payment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class CheckoutsController extends Controller{
    public function index(){
        $payments = Payment::orderBy('priority','asc')->get();
         return view('frontend.pages.checkouts',compact('payments'));
    }

    public function store(Request $request){
        $this->validate($request,[
            'name'    => 'required',
            'phone_number'    => 'required',
            'shipping_address'    => 'required',
            'payment_method_id'    => 'required',
        ]);

        $order = new Order();

//        check transaction_id given or not
        if ($request->payment_method_id != 'cash_in'){
            if ($request->transaction_id == NULL OR empty($request->transaction_id)){
                session()->flash('errormsg','Please give a Transaction ID for your payment');
                return back();
            }
        }

        $order->name = $request->name;
        $order->email = $request->email;
        $order->phone_number = $request->phone_number;
        $order->shipping_address = $request->shipping_address;
        $order->message = $request->message;
        $order->transaction_id = $request->transaction_id;
        $order->transaction_id = $request->transaction_id;
        $order->ip_address = request()->ip();
        if (Auth::check()){
            $order->user_id = Auth::id();
        }
        $order->payment_id = Payment::where('short_name',$request->payment_method_id)->first()->id;
        $order->save();

        foreach (Cart::totalCartItems() as $cart){
            $cart->order_id = $order->id;
            $cart->save();
        }

        session()->flash('success','Your Oder your order is taken successfully!!! Please wait admin will confirm it.');
        return redirect()->route('index');
    }
}
