<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Cart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;


class CartsController extends Controller{

    public function index(){
        return view('frontend.pages.carts');
    }

    public function store(Request $request){
        $this->validate($request,[
            'product_id' => 'required'
        ],
        [
            'product_id.required' => 'Please select a product'
        ]);

        if (Auth::check()){
            $cart = Cart::where('user_id',Auth::id())
                  ->where('product_id',$request->product_id)
                  ->where('order_id',NULL)
                  ->first();
        }else{
            $cart = Cart::where('ip_address',request()->ip())
                ->where('product_id',$request->product_id)
                ->where('order_id',NULL)
                ->first();
        }

        if (!is_null($cart)){
            $cart->increment('quantity');
            session()->flash('success','Product has added to card.');
            return back();
        }else{
            $cart = new  Cart();
            if (Auth::check()){
                $cart->user_id = Auth::id();
                $cart->ip_address = request()->ip();
                $cart->product_id = $request->product_id;
                $cart->save();
            }else{
                $cart->user_id = Auth::id();
                $cart->ip_address = request()->ip();
                $cart->product_id = $request->product_id;
                $cart->save();

                session()->flash('success','Product has added to card.');
                return back();
            }
            session()->flash('success','Product has added to card.');
            return back();
        }
    }

    public function update(Request $request,$id){
        $cart = Cart::find($id);
        if (!is_null($cart)){
           $cart->quantity = $request->quantity;
           $cart->save();
        }else{
           return redirect()->route('carts');
        }

        session()->flash('success','Cart items has Updated');
        return back();
    }

    public function delete($id){
        $cart = Cart::find($id);
        if (!is_null($cart)){
            $cart->delete();
        }else{
            return redirect()->route('carts');
        }
        session()->flash('success','Cart items has Updated');
        return back();
    }
}
