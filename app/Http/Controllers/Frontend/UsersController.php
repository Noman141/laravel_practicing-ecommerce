<?php

namespace App\Http\Controllers\Frontend;

use App\Models\District;
use App\Models\Division;
use foo\bar;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller{


    public function __construct(){
        $this->middleware('auth');
    }

    public function dashboard(){
        $user = Auth::user();
         return view('frontend.pages.users.dashboard',compact('user'));
    }

    public function userProfile(){
        $divisions = Division::orderBy('name','asc')->get();
        $districts = District::orderBy('name','asc')->get();
        $user = Auth::user();
        return view('frontend.pages.users.profile',compact('user','divisions','districts'));
    }

    public function userProfileUpdate(Request $request){

        $user = Auth::user();

        $request->validate([
            'first_name' => 'required| string| max:255',
            'last_name' => 'required| string| max:255',
            'user_name' => 'required| string| max:100|unique:users,user_name,'.$user->id,
            'email' => 'required| string| email| max:255| unique:users,email,'.$user->id,
            'phone_number' => 'required| max:15|unique:users,user_name,'.$user->id,
            'division_id' => 'required| numeric',
            'password' => 'string|nullable',
            'district_id' => 'required |numeric',
            'street_address' => 'required |max:255',
            'shipping_address' => 'required| max:255',
        ]);


        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->user_name = $request->user_name;
        $user->email = $request->email;
        if ($user->password != NULL OR $user->password !=""){
            $user->password = Hash::make($request->password);
        }
        $user->phone_number = $request->phone_number;
        $user->division_id = $request->division_id;
        $user->district_id = $request->district_id;
        $user->street_address = $request->street_address;
        $user->shipping_address = $request->shipping_address;
        $user->ip_address = request()->ip();

        $user->save();

        session()->flash('success','User Profile has Updated Successfully');

        return back();
    }
}
