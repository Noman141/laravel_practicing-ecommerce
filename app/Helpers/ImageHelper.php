<?php
/**
 * Created by PhpStorm.
 * User: Noman141
 * Date: 2/5/2019
 * Time: 10:08 PM
 * class Name: Image helper
 */

namespace App\Helpers;

use App\User;
use App\Helpers\GravatarHelper;

class ImageHelper{

    public static function getUserImage($id){
        $user = User::find($id);
        $avatar_url = "";

        if (!is_null($user)){
            if ($user->avatar == NULL){
//                Return im gravatar image
                if (GravatarHelper::validateGravatar($user->email)){
                    $avatar_url = GravatarHelper::gravatarImage($user->email,100);
                }else{
//                 when there is no gravatar image
                    $avatar_url = url('img/default/user.png');
                }
            }else{
//            Return This image
                $avatar_url = url('img/users/'.$user->avatar);
            }
        }else{
//         return url
        }
        return $avatar_url;
    }
}