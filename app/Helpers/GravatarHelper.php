<?php
/**
 * Created by PhpStorm.
 * User: anoma
 * Date: 2/5/2019
 * Time: 9:45 PM
 * gravatar helper
 */

namespace App\Helpers;

class GravatarHelper{

    /********
     * Validate gravatar image
     *
     * check if the user have any gravatar image
     *
     * @param string $email Email of user
     * @return bool true, If there is an image else, otherwise
     *
     */

    public static function validateGravatar($email){
        $hash = md5($email);
        $url = 'https://gravatar.com/avatar/'.$hash.'?d=404';
        $headers = @get_headers($url);
        if (!preg_match("|200|",$headers[0])){
            $has_valid_avatar = false;
        }else{
            $has_valid_avatar = true;
        }
        return $has_valid_avatar;
    }




    /**
     * Gravatar Image
     *
     * Get the gravatar image from an email address
     *
     * @param String $email User email
     * @param integer $size size of image
     * @param string $d type of image if not gravatar image
     * @return string Gravatar image
    */

    public static function gravatarImage($email ,$size=0, $d=""){
        $hash = md5($email);
        $image_url = 'https://gravatar.com/avatar/'.$hash.'?s='.$size.'&d='.$d;
        return $image_url;
    }
}