<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Division extends Model
{
    public function districts(){
        return $this->hasMany(\App\Models\District::class);
    }
}
