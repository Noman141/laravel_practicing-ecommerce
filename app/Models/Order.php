<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Order extends Model{
    //

    public $fillable = [
        'user_id','payment_id','name','email','phone_number','shipping_address','message','ip_address','is_paid','is_competed','is_seen_by_admin','	transaction_id'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function carts(){
        return $this->belongsTo(Cart::class);
    }

    public function cartItems(){
        return $this->hasMany(Cart::class);
    }

    public function payment(){
        return $this->belongsTo(Payment::class);
    }
}
