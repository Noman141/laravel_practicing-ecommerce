<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model{
    public $fillable = [
        'title','image','email','button_text','button_link','priority'
    ];
}
