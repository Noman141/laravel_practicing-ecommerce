<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Cart extends Model
{
    public $fillable= [
        'product_id','order_id','user_id','ip_address','quantity'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function oder(){
        return $this->belongsTo(Order::class);
    }

    public function product(){
        return $this->belongsTo(Product::class);
    }

    /**
     * total carts items
     * @return int (total carts)
     */
    public static function totalCarts(){
        if (Auth::check()){
            $carts = Cart::orwhere('user_id',Auth::id())
                  ->where('ip_address', request()->ip())
                  ->where('order_id',NULL)
                  ->get();

        }else{
            $carts = Cart::where('ip_address', request()->ip())->where('order_id',NULL)->get();
        }

        $total_item = 0;
        foreach ($carts as $cart){
            $total_item += $cart->quantity;
        }
        return $total_item;
    }

    /**
     * total carts
     * @return int
     */
    public static function totalCartItems(){
        if (Auth::check()){
            $carts = Cart::where('user_id',Auth::id())
                ->where('order_id',NULL)
                ->get();

        }else{
            $carts = Cart::where('ip_address', request()->ip())->where('order_id',NULL)->get();
        }
        return $carts;
    }
}
