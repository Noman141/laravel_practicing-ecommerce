<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Home Page route for front-end
Route::get('/','Frontend\PagesController@index')->name('index');

//contact route for front-end
Route::get('/contact','Frontend\PagesController@contact')->name('contact');

//User routes for frontend starts
Route::group(['prefix' => 'user'], function() {

    Route::get('/token/{token}', 'Frontend\VerificationController@verify')->name('user.verification');

    Route::get('/dashboard', 'Frontend\UsersController@dashboard')->name('user.dashboard');

    Route::get('/profile', 'Frontend\UsersController@userProfile')->name('user.profile');

    Route::post('/profile/update', 'Frontend\UsersController@userProfileUpdate')->name('user.profile.update');

});
//User routes for frontend ends

//checkout routes for frontend starts
Route::group(['prefix' => 'checkout'], function() {

    Route::get('/', 'Frontend\CheckoutsController@index')->name('checkouts');

    Route::post('/store', 'Frontend\CheckoutsController@store')->name('checkouts.store');

});
//checkout routes for frontend ends

//Products route for front-end
Route::group(['prefix' => 'products'], function() {
    Route::get('/', 'Frontend\ProductsController@index')->name('products');

    Route::get('/{slug}', 'Frontend\ProductsController@show')->name('product.show');

    Route::get('/new/search', 'Frontend\PagesController@search')->name('product.search');

    Route::get('/categories', 'Frontend\CategoriesController@index')->name('product.categories.index');

    Route::get('/category/{id}', 'Frontend\CategoriesController@show')->name('product.category.show');

//    Category routes starts
});

//backend routes Stat from  hare******************************************************
Route::group(['prefix' => 'backend'], function(){

//    backend index routes starts
    Route::get('/','Backend\PagesController@index')->name('admin.index');

//    Admin routes

    Route::get('/login','Auth\Admin\LoginController@showLoginForm')->name('admin.login');

    Route::post('/login/submit','Auth\Admin\LoginController@login')->name('admin.login.submit');

    Route::post('/logout','Auth\Admin\LoginController@logout')->name('admin.logout');
//  password reset email
    Route::get('/password/reset','Auth\Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');

    Route::post('/password/reset/pass','Auth\Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');

    //  password reset
    Route::get('/password/reset/{token}','Auth\Admin\ResetPasswordController@showResetForm')->name('admin.password.reset');

    Route::post('/password/reset','Auth\Admin\ResetPasswordController@reset')->name('admin.password.update');

//  Products routes for backend panel starts
    Route::group(['prefix' => 'product'], function(){

        Route::get('/','Backend\ProductController@index')->name('admin.manage_product');

        Route::get('/create','Backend\ProductController@create')->name('admin.product.create');

        Route::post('/store','Backend\ProductController@store')->name('admin.product.store');


        Route::get('/edit/{id}','Backend\ProductController@edit')->name('admin.product.edit');

        Route::post('/update/{id}','Backend\ProductController@update')->name('admin.product.update');

        Route::get('/delete/{id}','Backend\ProductController@delete')->name('admin.product.delete');
    });
    //  Products routes ends

    //  Orders routes for backend panel starts
    Route::group(['prefix' => 'order'], function(){

        Route::get('/','Backend\OrdersController@index')->name('admin.manage_order');

        Route::get('/view/{id}','Backend\OrdersController@show')->name('admin.order.show');

        Route::get('/delete/{id}','Backend\OrdersController@delete')->name('admin.order.delete');

        Route::post('/completed/{id}','Backend\OrdersController@completed')->name('admin.order.completed');

        Route::post('/paid/{id}','Backend\OrdersController@paid')->name('admin.order.paid');
    });
    //  Orders routes ends

    //  Category routes for backend panel starts
    Route::group(['prefix' => 'category'], function(){

        Route::get('/','Backend\CategoryController@index')->name('admin.manage_category');

        Route::get('/create','Backend\CategoryController@create')->name('admin.category.create');

        Route::post('/store','Backend\CategoryController@store')->name('admin.category.store');

        Route::get('/edit/{id}','Backend\CategoryController@edit')->name('admin.category.edit');

        Route::post('/update/{id}','Backend\CategoryController@update')->name('admin.category.update');

        Route::get('/delete/{id}','Backend\CategoryController@delete')->name('admin.category.delete');

    });
//    Category routes ends

    //  Brand routes for backend panel starts
    Route::group(['prefix' => 'brand'], function(){

        Route::get('/','Backend\BrandController@index')->name('admin.manage_brand');

        Route::get('/create','Backend\BrandController@create')->name('admin.brand.create');

        Route::post('/store','Backend\BrandController@store')->name('admin.brand.store');

        Route::get('/edit/{id}','Backend\BrandController@edit')->name('admin.brand.edit');

        Route::post('/update/{id}','Backend\BrandController@update')->name('admin.brand.update');

        Route::get('/delete/{id}','Backend\BrandController@delete')->name('admin.brand.delete');
    });
//    Brand routes ends

    //  District routes for backend panel starts
    Route::group(['prefix' => 'district'], function(){

        Route::get('/','Backend\DistrictController@index')->name('admin.manage_district');

        Route::get('/create','Backend\DistrictController@create')->name('admin.district.create');

        Route::post('/store','Backend\DistrictController@store')->name('admin.district.store');

        Route::get('/edit/{id}','Backend\DistrictController@edit')->name('admin.district.edit');

        Route::post('/update/{id}','Backend\DistrictController@update')->name('admin.district.update');

        Route::get('/delete/{id}','Backend\DistrictController@delete')->name('admin.district.delete');
    });
//    District routes ends

    //  Division routes for backend panel starts
    Route::group(['prefix' => 'division'], function(){

        Route::get('/','Backend\DivisionController@index')->name('admin.manage_division');

        Route::get('/create','Backend\DivisionController@create')->name('admin.division.create');

        Route::post('/store','Backend\DivisionController@store')->name('admin.division.store');

        Route::get('/edit/{id}','Backend\DivisionController@edit')->name('admin.division.edit');

        Route::post('/update/{id}','Backend\DivisionController@update')->name('admin.division.update');

        Route::get('/delete/{id}','Backend\DivisionController@delete')->name('admin.division.delete');
    });
//    Division routes ends

    //  Slider routes for backend panel starts
    Route::group(['prefix' => 'slider'], function(){

        Route::get('/','Backend\SlidersController@index')->name('admin.manage_slider');

        Route::post('/store','Backend\SlidersController@store')->name('admin.slider.store');

        Route::post('/update/{id}','Backend\SlidersController@update')->name('admin.slider.update');

        Route::get('/delete/{id}','Backend\SlidersController@delete')->name('admin.slider.delete');
    });
//    Slider routes ends

//backend routs ends
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


// Api Routes

Route::get('get-district/{id}',function ($id){
    return json_encode(App\Models\District::where('division_id',$id)->get());
});