@extends('frontend.pages.product.layouts.master')
@section('title')
    Neer Shop - CheckOuts
@endsection

@section('content')
<div class="container">
    <div class="text-center">
        @include('backend.partials.errormessage')
    </div>
    <div class="card card-body">
        <h2>Confirm Items</h2>
        <hr>
        <div class="row">
            <div class="col-md-7">
                @foreach(App\Models\Cart::totalCartItems() as $cart)
                    <p>
                        {{ $cart->product->title }} -
                        <strong>{{ $cart->product->price }} $</strong> for
                        {{ $cart->quantity }} Items
                    </p>
                @endforeach
            </div>
            <div class="col-md-5">
                @php  $total_price = 0 @endphp
                @foreach(App\Models\Cart::totalCartItems() as $cart)
                    @php
                       $total_price += $cart->product->price * $cart->quantity
                    @endphp
                @endforeach
                <p>Total price: <strong>{{ $total_price }} $</strong></p>
                <p>Total price with Shipping Charge: <strong>{{ $total_price + App\Models\Setting::first()->shipping_cost}} $</strong></p>
            </div>
        </div>
        <p>
            <a href="{{ route('carts') }}"> Change Carts</a>
        </p>
    </div>
    <div class="card card-body my-3">
        <h2>Confirm Shipping Address</h2>
        <hr>
        <form method="POST" action="{{ route('checkouts.store') }}">
            @csrf
            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                <div class="col-md-6">
                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ Auth::check() ? Auth::user()->first_name.' '. Auth::user()->last_name :'' }}" required autofocus>

                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                <div class="col-md-6">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{  Auth::check() ? Auth::user()->email :'' }}" required>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="phone_number" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>

                <div class="col-md-6">
                    <input id="phone_number" type="text" class="form-control{{ $errors->has('phone_number') ? ' is-invalid' : '' }}" name="phone_number" value="{{ Auth::check() ? Auth::user()->phone_number :'' }}" required>

                    @if ($errors->has('phone_number'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('phone_number') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="shipping_address" class="col-md-4 col-form-label text-md-right">{{ __('Shipping Address') }}</label>

                <div class="col-md-6">
                    <textarea id="shipping_address" type="text" class="form-control{{ $errors->has('shipping_address') ? ' is-invalid' : '' }}" name="shipping_address" >{{  Auth::check() ? Auth::user()->shipping_address :'' }}</textarea>

                    @if ($errors->has('shipping_address'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('shipping_address') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="message" class="col-md-4 col-form-label text-md-right">{{ __('Aditional Message (Optional)') }}</label>

                <div class="col-md-6">
                    <textarea id="message" type="text" class="form-control{{ $errors->has('message') ? ' is-invalid' : '' }}" name="message" ></textarea>

                    @if ($errors->has('message'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('message') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="payment_method" class="col-md-4 col-form-label text-md-right">{{ __('Payment Method') }}</label>
                <div class="col-md-6">
                    <select class="browser-default custom-select" name="payment_method_id" id="payment_method" required>
                      <option value="" disabled selected>Select One---</option>
                        @foreach($payments as $payment)
                        <option value="{{ $payment->short_name }}">{{ $payment->name }}</option>
                        @endforeach
                    </select>

                    <div class="bg-info">
                        @foreach($payments as $payment)
                            @if($payment->short_name == 'cash_in')
                                <div id="payment_{{ $payment->short_name }}" class="hidden card card-body mt-3 bg-info text-center">
                                    <h5>For Cash in delivery There is nothing necessary. Just click finish Oder button. </h5>
                                    <small>You will get your products with in two ir three days</small>
                                </div>
                            @else
                                <div id="payment_{{ $payment->short_name }}" class="hidden card card-body mt-3 bg-info text-center">
                                    <h3>{{ $payment->name }} Payment</h3>
                                    <span>
                                    <strong>{{ $payment->name }} No : {{ $payment->no }}</strong><br>
                                    <strong>Account Type: {{ $payment->type }}</strong>
                                </span><br/>
                                    <div class="alert alert-success">Please send above money to this {{ $payment->name }} No and write your transaction ID</div>
                                </div>
                            @endif
                        @endforeach
                        <input type="text" name="transaction_id" id="transaction_id" class="form-control hidden " placeholder="Enter Transaction ID">
                    </div>

                    @if ($errors->has('payment_method'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('payment_method') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row ">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary btn-sm">
                        {{ __('Order Now') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $("#payment_method").change(function () {
            $payment_method = $("#payment_method").val();
            // alert($payment_method)
            if ($payment_method == 'cash_in'){
                $("#payment_cash_in").removeClass("hidden")
                $("#payment_bkash").addClass("hidden")
                $("#payment_rocket").addClass("hidden")
            }else if($payment_method == 'bkash'){
                $("#payment_bkash").removeClass("hidden")
                $("#payment_cash_in").addClass("hidden")
                $("#payment_rocket").addClass("hidden")
                $("#transaction_id").removeClass("hidden")
            }else if($payment_method == 'rocket'){
                $("#payment_rocket").removeClass("hidden")
                $("#payment_cash_in").addClass("hidden")
                $("#payment_bkash").addClass("hidden")
                $("#transaction_id").removeClass("hidden")
            }
        })
    </script>
@endsection