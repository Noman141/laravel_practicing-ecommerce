@extends('frontend.pages.product.layouts.master')
@section('title')
    Neer Shop - Carts
@endsection

@section('content')
    <div class="container">
        @if(App\Models\Cart::totalCarts() > 0)
            <h1>My Cart Items</h1>
            @include('backend.partials.errormessage')
            <table class="table table-striped">
                <thead class="black white-text">
                <tr>
                    <th width="5%">No.</th>
                    <th width="15%"> Name</th>
                    <th width="20%"> Image</th>
                    <th width="30%"> Quantity</th>
                    <th width="10%">Unit Price</th>
                    <th width="10%">Total Price</th>
                    <th width="10%">Action</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $total_price = 0;
                @endphp
                @foreach(App\Models\Cart::totalCartItems() as $cart)
                    <tr>
                        <th >{{ $loop->index +1 }}</th>
                        <td><a href="{{ route('product.show',$cart->product->slug) }}">{{ $cart->product->title }}</a></td>
                        <td>
                            @if($cart->product->images->count() > 0)
                                <img src="{{ asset('img/products/'.$cart->product->images->first()->image) }}" alt="{{ $cart->product->title }}" width="60px">
                            @endif
                        </td>
                        <td>
                            <form action="{{ route('carts.update',$cart->id) }}" class="form-inline" method="post">
                                @csrf
                                <input type="number" name="quantity" value="{{ $cart->quantity }}">
                                <button type="submit" class="btn btn-info btn-sm">Update</button>
                            </form>
                        </td>
                        <td>{{ $cart->product->price }}$</td>
                        <td>
                            @php
                                $total_price += $cart->product->price * $cart->quantity;
                            @endphp
                            {{ $cart->product->price * $cart->quantity}}$
                        </td>
                        <th scope="col">
                            <form action="{{ route('carts.delete',$cart->id) }}" class="form-inline" method="post">
                                @csrf
                                <input type="hidden" name="cart_id">
                                <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                            </form>
                        </th>
                    </tr>
                @endforeach
                <tr class="text-danger">
                    <td colspan="4"></td>
                    <td><b>Total Amount</b></td>
                    <td><b>{{$total_price}}$</b></td>
                    <td></td>
                </tr>
                </tbody>
            </table>
            <div class="float-right">
                <a href="{{ route('products') }}" class="btn btn-info ">Continue Shopping...</a>
                <a href="{{ route('checkouts') }}" class="btn btn-warning ">Check Out</a>
            </div>
        @else
            <div class="alert alert-warning text-center">
                <h2>There is no Items in your cart.</h2>
                <a href="{{ route('products') }}" class="btn btn-info ">Continue Shopping...</a>
            </div>
        @endif
    </div>
@endsection