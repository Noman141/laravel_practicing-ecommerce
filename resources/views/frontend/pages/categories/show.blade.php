@extends('frontend.layouts.master')
@extends('frontend.partials.styles')
{{--for page styles--}}

@section('title')
    Neer Shop - Products - {{ $category->name }}
@endsection

@section('style')

@endsection


{{--for page content--}}
@section('content')
    <div class="widget">
        <h2>All Products in <strong> {{ $category->name }}</strong></h2>

        @php
          $products = $category->products()->paginate(9);
        @endphp

        @if($products->count() > 0)
            @include('frontend.pages.product.partials.all')
        @else
             <div class="alert alert-warning">
                 No Products is Here,
             </div>
         @endif
        @include('frontend.pages.product.partials.all')
    </div>


    <div class="pagination mt-5">
        {{ $products->links() }}
    </div>
@endsection

{{--for page js--}}
@section('scripts')
    <script></script>
@endsection