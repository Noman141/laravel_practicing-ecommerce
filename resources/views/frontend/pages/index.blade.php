@extends('frontend.layouts.master')
@section('title')
    Neer Shop - Home
@endsection
{{--for page styles--}}
@section('style')

@endsection


{{--for page content--}}
@section('content')
<div class="our-slider">
    <!--Carousel Wrapper-->
    <div id="carousel-example-2" class="carousel slide carousel-fade" data-ride="carousel">
        <!--Indicators-->
        <ol class="carousel-indicators">
            @foreach($sliders as $slider)
            <li data-target="#carousel-example-2" data-slide-to="{{$loop->index}}" class="{{ $loop->index == 0?'active':'' }}"></li>
            @endforeach

        </ol>
        <!--/.Indicators-->
        <!--Slides-->
        <div class="carousel-inner" role="listbox">
            @foreach($sliders as $slider)
            <div class="carousel-item {{ $loop->index == 0?'active':'' }}">
                <div class="view">
                    <img class="d-block w-100" src="{{ asset('img/sliders/'.$slider->image) }}" alt="{{ $slider->title }}">
                    <div class="mask rgba-black-light"></div>
                </div>
                <div class="carousel-caption">
                    <h3 class="h3-responsive">{{ $slider->title }}</h3>
                    @if($slider->button_text)
                    <a href="{{ $slider->button_link }}" class="btn btn-sm btn-info">{{ $slider->button_text }}</a>
                    @endif
                </div>
            </div>
            @endforeach
        </div>
        <!--/.Slides-->
        <!--Controls-->
        <a class="carousel-control-prev" href="#carousel-example-2" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carousel-example-2" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
        <!--/.Controls-->
    </div>
    <!--/.Carousel Wrapper-->
</div>

<div class="widget mt-5">
    <hr>
    <h2>Featured Products</h2>
        @include('backend.partials.errormessage')
        @include('frontend.pages.product.partials.all')

</div>


<div class="pagination mt-5">
    {{ $products->links() }}
</div>
@endsection

{{--for page js--}}
@section('scripts')
<script></script>
@endsection