<div class="row m-0">

    @foreach($products as $product)
        <div class="col-md-4 mt-3">
            <!-- Card -->
            <div class="card">

                <!--Card image-->
                <div class="view overlay">
                    
                    @php $i = 1;  @endphp
                    @foreach($product->images as $image)
                        @if($i > 0 )
                            <a href="{{ route('product.show',$product->slug) }}"><img class="card-img-top" src="{{ asset('img/products/'.$image->image) }}" alt="{{ $product->title }}"></a>
                        @endif
                        @php $i--; @endphp
                    @endforeach
                    <a href="{{ route('product.show',$product->slug) }}">
                        <div class="mask rgba-white-slight"></div>
                    </a>
                </div>

                <!--Card content-->
                <div class="card-body">

                    <!--Title-->
                    <a href="{{ route('product.show',$product->slug) }}"><h4 class="card-title">{{ $product->title }}</h4></a>
                    <!--Text-->
                    <p class="card-text">Price: {{ $product->price }}</p>
                    <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->

                    @include('frontend.pages.product.partials.cart_button')

                    {{--<a href="{{ route('product.show',$product->slug) }}" class="btn btn-warning btn-rounded waves-effect btn-sm">View Details</a>--}}
                </div>

            </div>
            <!-- Card -->
        </div>
    @endforeach
</div>