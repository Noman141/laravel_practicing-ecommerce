<form class="form-inline" method="post">
    {{--@csrf--}}
    <input type="hidden" name="product_id" value="{{ $product->id }}">
    <button type="button" onclick="addToCart({{$product->id}})" class="btn btn-warning btn-sm"><i class="fas fa-plus"></i> Add To Cart</button>
</form>