@extends('frontend.pages.product.layouts.master')
@extends('frontend.partials.styles')
{{--for page styles--}}

@section('title')
    Neer Shop - Products Details
@endsection

@section('style')

@endsection


{{--for page content--}}
@section('content')

   <div class="container">
       <h2 class="text-center my-3">Product Details</h2>
       <hr>
       <div class="row mb-3">
           <div class="col-md-4">
               <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                   <div class="carousel-inner">
                       @php $i = 1; @endphp
                       @foreach($product->images as $image)
                       <div class="carousel-item {{ $i ==1?'active':'' }}">
                           <img class="d-block w-100" src="{!! asset('img/products/'.$image->image) !!}" alt="{{ $image->image }}">
                       </div>
                       @php $i++; @endphp
                       @endforeach
                   </div>
                   <a class="carousel-control-prev text-info" href="#carouselExampleControls" role="button" data-slide="prev">
                       <span class="carousel-control-prev-icon control-icon" aria-hidden="true"><i class="fas fa-angle-double-left"></i></span>
                       <span class="sr-only">Previous</span>
                   </a>
                   <a class="carousel-control-next text-info" href="#carouselExampleControls" role="button" data-slide="next">
                       <span class="carousel-control-next-icon control-icon" aria-hidden="true"><i class="fas fa-angle-double-right"></i></span>
                       <span class="sr-only">Next</span>
                   </a>
               </div>
           </div>
           <div class="col-md-8">
               <div class="widget">
                   <h4>Product Name : <strong> {{ $product->title }} </strong></h4>
                   <h4>Product Price : <strong>{{ $product->price }}Tk </strong></h4>
                   <h4>Product Brand : <strong>{{ $product->brand->name }} </strong></h4>
                   <h4>Product Category : <strong> {{ $product->category->name }} </strong></h4>
                   <h4>Product Description : <small> {{$product->description}} </small></h4>
               </div>
           </div>
       </div>
   </div>

@endsection

{{--for page js--}}
@section('scripts')
    <script></script>
@endsection