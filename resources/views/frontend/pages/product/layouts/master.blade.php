<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>
        @yield('title')
    </title>
    {{--css styles--}}
    @include('frontend.partials.styles')
</head>
<body>
<header>
    {{--navber--}}
    @include('frontend.partials.nav')
    {{--navber--}}
</header>

{{--sedebar--}}

@yield('content')


{{--footer--}}
@include('frontend.partials.footer')
{{--footer--}}

{{--secipts--}}
@include('frontend.partials.scripts')
{{--secipts--}}
</body>
</html>