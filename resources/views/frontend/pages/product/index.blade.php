@extends('frontend.layouts.master')
@extends('frontend.partials.styles')
{{--for page styles--}}

@section('title')
    Neer Shop - Products
@endsection

@section('style')

@endsection


{{--for page content--}}
@section('content')
    <div class="widget">
        <h2>All Products</h2>
        @include('frontend.pages.product.partials.all')
    </div>


    {{--<div class="pagination mt-5">--}}
        {{--{{ $products->links() }}--}}
    {{--</div>--}}
@endsection

{{--for page js--}}
@section('scripts')
    <script></script>
@endsection