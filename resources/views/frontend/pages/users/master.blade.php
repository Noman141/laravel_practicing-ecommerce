@extends('frontend.pages.product.layouts.master')
@extends('frontend.partials.styles')

@section('content')
    <div class="container mt-3">
        <div class="row">
            <div class="col-md-4">
               <div class="card">
                   <div class="card-body">
                       <div class="list-group">
                           <a class="list-group-item text-center">
                               <img class="rounded-circle " src="{{ App\Helpers\ImageHelper::getUserImage(Auth::user()->id) }}" alt="{{ Auth::user()->last_name }}" >
                           </a>
                           <a href="{{ route('user.dashboard') }}" class="list-group-item {{ Route::currentRouteName() == 'user.dashboard' ? 'active' : '' }}">Dashboard</a>
                           <a href="{{ route('user.profile') }}" class="list-group-item {{ Route::currentRouteName() == 'user.profile' ? 'active' : '' }}">Update Profile</a>
                           <a href="" class="list-group-item">Log Out</a>
                       </div>
                   </div>
               </div>
            </div>
            <div class="col-md-8">
                <div class="card mb-5">

                        @yield('sub-content')

                </div>
            </div>
        </div>
    </div>
@endsection