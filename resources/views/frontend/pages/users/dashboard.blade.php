@extends('frontend.pages.users.master')

@section('sub-content')
    <h2>Welcome {{ $user->first_name .' '. $user->last_name}} </h2>
    <p>You Can Change Your Profile And Every Information From Here</p>
@endsection
