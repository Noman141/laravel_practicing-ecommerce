<div class="container">
    <div class="row mt-3">
        <div class="col-md-3">
            <h2 class="">Categories</h2>
            <ul class="list-group list-group-flush">
                @foreach(App\Models\Category::orderBy('id','asc')->where('parent_id',NULL)->get() as $parent)
                    <li class="list-group-item">
                        <a href="#main-{{ $parent->id }}" data-toggle="collapse" class=" list-group-item-action">
                            <img src="{{ asset('img/category/'.$parent->image) }}" alt="" width="50px;">
                            {{ $parent->name }}
                        </a>
                    </li>
                    <div class="collapse
                           @if(Route::is('product.category.show'))
                               @if(App\Models\Category::ParentOrNotCategory($parent->id,$category->id))
                                  show
                               @endif
                            @endif
                        " id="main-{{ $parent->id }}">
                        <div class="child-rows">
                            @foreach(App\Models\Category::orderBy('id','asc')->where('parent_id',$parent->id)->get() as $child)
                                <li class=" ml-5" style="list-style-type: none">
                                    <a href="{!!route('product.category.show',$child->id )!!}" class="list-group-item list-group-item-action
                                          @if(Route::is('product.category.show') AND $child->id == $category->id)
                                                acttive
                                          @endif
                                       ">
                                        <img src="{{ asset('img/category/'.$child->image) }}" alt="" width="30px;">
                                        {{ $child->name }}
                                    </a>
                                </li>
                            @endforeach
                        </div>
                    </div>
                @endforeach
            </ul>
        </div>
        <div class="col-md-9">
            @yield('content')
        </div>
    </div>
</div>