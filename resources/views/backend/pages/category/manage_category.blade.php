@extends('backend.layouts.master')

@section('content')

    <div class="table-responsive text-nowrap">
        <h2 class="mt-3">Manage Products</h2>
        <hr>
        @include('backend.partials.errormessage')
        <table class="table">
            <thead>
            <tr>
                <th scope="col">No.</th>
                <th scope="col">Name</th>
                <th scope="col">Description</th>
                <th scope="col">image</th>
                <th scope="col">Parent Category</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>

            @php $i = 0; @endphp
            @foreach($categories as $category)
                @php $i++; @endphp
                <tr>
                    <th scope="row">{{ $i}}</th>
                    <td>{{ $category->name }}</td>
                    <td>{{ $category->description }}</td>
                    <td>
                        <img src="{{ asset('img/category/'.$category->image) }}" alt="" width="100px;" height="80px;">
                    </td>
                    <td>
                        @if($category->parent_id == NULL)
                            Primary Category
                        @else
                            {{ $category->parent->name }}
                        @endif

                    </td>
                    <td>
                        <a class="btn btn-success btn-small" href="{{ route('admin.category.edit',$category->id) }}">Edit</a>
                        <a class="btn btn-danger btn-small" href="#deleteCategory{{ $category->id }}" data-toggle="modal" data-target="">Delete</a>

                        <!--Delete Modal -->
                        <div class="modal fade text-center" id="deleteCategory{{ $category->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
                             aria-hidden="true">

                            <!-- Add .modal-dialog-centered to .modal-dialog to vertically center the modal -->
                            <div class="modal-dialog modal-dialog-centered" role="document">


                                <div class="modal-content">
                                    <div class="modal-header text-center">
                                        <h4 class="modal-title text-danger" id="exampleModalLabel" >Delete!</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <h4 class=" text-danger" >Are You Sure To Delete!!!</h4>
                                    </div>
                                    <div class="modal-footer">
                                        <a href="{{ route('admin.category.delete',$category->id) }}" class="btn btn-danger" >Yes</a>
                                        <button  type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection