@extends('backend.layouts.master')

@section('content')
    <div class="container" style="width: 700px; margin: 0 auto;font-size: 30px !important;">
        <!-- Horizontal material form -->
        <h2 class="mt-3">Add Category</h2>
        <hr>
        {{--Validation message--}}
        @include('backend.partials.errormessage')
        <form action="{{ route('admin.category.update',$category->id) }}" method="post" enctype="multipart/form-data">
        @csrf
        <!-- Grid row -->
            <div class="form-group row">
                <!-- Material input -->
                <label for="name" class="col-sm-3 col-form-label">Name</label>
                <div class="col-sm-9">
                    <div class="md-form mt-0">
                        <input type="text" name="name" class="form-control" id="name" value="{{ $category->name }}">
                    </div>
                </div>
            </div>
            <!-- Grid row -->

            <!-- Grid row -->
            <div class="form-group row">
                <!-- Material input -->
                <label for="description" class="col-sm-3 col-form-label">Descriptions</label>
                <div class="col-sm-9">
                    <div class="md-form mt-0">
                        <textarea name="description" class="form-control" id="description" rows="5">
                            {{ $category->description }}
                        </textarea>
                    </div>
                </div>
            </div>
            <!-- Grid row -->

            <!-- Grid row -->
            <div class="form-group row">
                <!-- Material input -->
                <label for="parent_category" class="col-sm-3 col-form-label">Parent Category</label>
                <div class="col-sm-9">
                    <div class="md-form mt-0" >
                        <select class="mdb-select form-control" name="parent_id">
                            <option disabled selected>Choose your option ---</option>
                            @foreach($main_category as $cat)
                                <option value="{{ $cat->id }}"{{ $cat->id == $category->parent_id ?'selected':NULL}}>{{ $cat->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <!-- Grid row -->
            <!-- Grid row -->
            <div class="form-group row">
                <!-- Material input -->
                <label class="col-sm-3 col-form-label">Your Old Image</label>
                <div class="col-sm-9">
                    <div class="md-form mt-0">
                        <img src="{{ asset('img/category/'.$category->image) }}" alt="" width="150px;" height="120px;">
                    </div>
                </div>
            </div>
            <!-- Grid row -->
            <!-- Grid row -->
            <div class="form-group row">
                <!-- Material input -->

                <label for="category-image" class="col-sm-3 col-form-label">Upload New Image</label>
                <div class="col-sm-9">
                    <div class="md-form mt-0">
                        <input type="file" name="image" class="form-control" id="category-image" placeholder="Choice File">
                    </div>
                </div>
            </div>
            <!-- Grid row -->

            <!-- Grid row -->
            <div class="form-group row">
                <label for="" class="col-sm-3 col-form-label"></label>
                <div class="col-sm-9">
                    <button type="submit" class="btn btn-primary btn-md">Update Category</button>
                </div>
            </div>
            <!-- Grid row -->
        </form>
        <!-- Horizontal material form -->
    </div>
@endsection