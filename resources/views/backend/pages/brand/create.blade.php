@extends('backend.layouts.master')

@section('content')
    <div class="container" style="width: 700px; margin: 0 auto;font-size: 30px !important;">
        <!-- Horizontal material form -->
        <h2 class="mt-3">Add Brand</h2>
        <hr>
        {{--Validation message--}}
        @include('backend.partials.errormessage')
        <form action="{{ route('admin.brand.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <!-- Grid row -->
            <div class="form-group row">
                <!-- Material input -->
                <label for="name" class="col-sm-3 col-form-label">Name</label>
                <div class="col-sm-9">
                    <div class="md-form mt-0">
                        <input type="text" name="name" class="form-control" id="name" placeholder="Name">
                    </div>
                </div>
            </div>
            <!-- Grid row -->

            <!-- Grid row -->
            <div class="form-group row">
                <!-- Material input -->
                <label for="description" class="col-sm-3 col-form-label">Descriptions</label>
                <div class="col-sm-9">
                    <div class="md-form mt-0">
                        <textarea name="description" class="form-control" id="description" placeholder="Descriptions" rows="5"></textarea>
                    </div>
                </div>
            </div>
            <!-- Grid row -->

            <!-- Grid row -->
            <div class="form-group row">
                <!-- Material input -->
                <label for="brand-image" class="col-sm-3 col-form-label">Upload Image</label>
                <div class="col-sm-9">
                    <div class="md-form mt-0">
                        <input type="file" name="image" class="form-control" id="brand-image" placeholder="Choice File">
                    </div>
                </div>
            </div>
            <!-- Grid row -->

            <!-- Grid row -->
            <div class="form-group row">
                <label for="" class="col-sm-3 col-form-label"></label>
                <div class="col-sm-9">
                    <button type="submit" class="btn btn-primary btn-md">Add Brand</button>
                </div>
            </div>
            <!-- Grid row -->
        </form>
        <!-- Horizontal material form -->
    </div>
@endsection