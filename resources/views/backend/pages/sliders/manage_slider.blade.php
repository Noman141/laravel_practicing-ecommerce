@extends('backend.layouts.master')

@section('content')

    <div class="table-responsive text-nowrap">
        <div class="row">
            <div class="col-md-6">
                <h2 class="mt-3">Manage Sliders</h2>
            </div>
            <div class="col-md-6">
                <a href="#addSlider" class="btn btn-sm btn-info float-right" data-toggle="modal" data-target="">Add Slider</a>
            </div>
            
        </div>
        <hr>
        @include('backend.partials.errormessage')
        <table class="table">
            <thead>
            <tr>
                <th scope="col">No.</th>
                <th scope="col">Title</th>
                <th scope="col">Image</th>
                <th scope="col">Priority</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>

            @php $i = 0; @endphp
            @foreach($sliders as $slider)
                @php $i++; @endphp
                <tr>
                    <th scope="row">{{ $i}}</th>
                    <td>{{ $slider->title }}</td>
                    <td><img src="{{ asset('img/sliders/'.$slider->image) }}" alt="" width="100px;" height="80px;"></td>
                    <td>{{ $slider->priority }}</td>
                    <td>
                        <a class="btn btn-success btn-small" href="#editSlider{{ $slider->id }}" data-toggle="modal" data-target="">Edit</a>
                        <a class="btn btn-danger btn-small" href="#deleteSlider{{ $slider->id }}" data-toggle="modal" data-target="">Delete</a>

                        <!--Add Modal -->
                        <div class="modal fade text-center" id="addSlider" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
                             aria-hidden="true">

                            <!-- Add .modal-dialog-centered to .modal-dialog to vertically center the modal -->
                            <div class="modal-dialog modal-dialog-centered" role="document">

                                <div class="modal-content">
                                    <div class="modal-header text-center">
                                        <h4 class="modal-title" id="exampleModalLabel" >Add New Slider</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <!-- Horizontal material form -->
                                        <form action="{{ route('admin.slider.store') }}" method="post" enctype="multipart/form-data">
                                            @csrf
                                          <!-- Grid row -->
                                          <div class="form-group row">
                                            <!-- Material input -->
                                            <label for="title" class="col-sm-4 col-form-label">Title</label>
                                            <div class="col-sm-8">
                                              <div class="md-form mt-0">
                                                <input type="text" class="form-control" name="title" id="title" placeholder="Title">
                                              </div>
                                            </div>
                                          </div>
                                          <!-- Grid row -->

                                          <!-- Grid row -->
                                          <div class="form-group row">
                                            <!-- Material input -->
                                            <label for="image" class="col-sm-4 col-form-label">Image</label>
                                            <div class="col-sm-8">
                                              <div class="md-form mt-0">
                                                <input type="file" class="form-control" id="image" name="image">
                                              </div>
                                            </div>
                                          </div>
                                          <!-- Grid row -->

                                          <!-- Grid row -->
                                          <div class="form-group row">
                                            <!-- Material input -->
                                            <label for="button_text" class="col-sm-4 col-form-label">Slider Button Text</label>
                                            <div class="col-sm-8">
                                              <div class="md-form mt-0">
                                                <input type="text" class="form-control" name="button_text" id="button_text" placeholder="Slider Button Text">
                                              </div>
                                            </div>
                                          </div>
                                          <!-- Grid row -->

                                          <!-- Grid row -->
                                          <div class="form-group row">
                                            <!-- Material input -->
                                            <label for="button_link" class="col-sm-4 col-form-label">Slider Button Link</label>
                                            <div class="col-sm-8">
                                              <div class="md-form mt-0">
                                                <input type="text" class="form-control" name="button_link" id="button_link" placeholder="Slider Button Link">
                                              </div>
                                            </div>
                                          </div>
                                          <!-- Grid row -->

                                          <!-- Grid row -->
                                          <div class="form-group row">
                                            <!-- Material input -->
                                            <label for="priority" class="col-sm-4 col-form-label">Priority</label>
                                            <div class="col-sm-8">
                                              <div class="md-form mt-0">
                                                <input type="text" class="form-control" name="priority" id="priority" placeholder="Priority">
                                              </div>
                                            </div>
                                          </div>
                                          <!-- Grid row -->

                                          <!-- Grid row -->
                                          <div class="form-group row">
                                            <label class="col-sm-4 col-form-label"></label>
                                            <div class="col-sm-8">
                                              <button type="submit" class="btn btn-primary btn-md">Add Slider</button>
                                            </div>
                                          </div>
                                          <!-- Grid row -->
                                        </form>
                                        <!-- Horizontal material form -->
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--Edit Modal -->
                        <div class="modal fade text-center" id="editSlider{{ $slider->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
                             aria-hidden="true">

                            <!-- Edit .modal-dialog-centered to .modal-dialog to vertically center the modal -->
                            <div class="modal-dialog modal-dialog-centered" role="document">

                                <div class="modal-content">
                                    <div class="modal-header text-center">
                                        <h4 class="modal-title" id="exampleModalLabel" >Edit New Slider</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <!-- Horizontal material form -->
                                        <form action="{{ route('admin.slider.update', $slider->id) }}" method="post" enctype="multipart/form-data">
                                            @csrf
                                          <!-- Grid row -->
                                          <div class="form-group row">
                                            <!-- Material input -->
                                            <label for="title" class="col-sm-4 col-form-label">Title</label>
                                            <div class="col-sm-8">
                                              <div class="md-form mt-0">
                                                <input type="text" class="form-control" name="title" id="title" value="{{ $slider->title }}">
                                              </div>
                                            </div>
                                          </div>
                                          <!-- Grid row -->

                                          <!-- Grid row -->
                                                <a href="{{ asset('img/sliders/'.$slider->image) }}">Click Here for view previous image</a>
                                          <div class="form-group row">
                                            <!-- Material input -->
                                            <label for="image" class="col-sm-4 col-form-label">Image</label>
                                            <div class="col-sm-8">
                                              <div class="md-form mt-0">
                                                <input type="file" class="form-control" id="image" name="image">
                                              </div>
                                            </div>
                                          </div>
                                          <!-- Grid row -->

                                          <!-- Grid row -->
                                          <div class="form-group row">
                                            <!-- Material input -->
                                            <label for="button_text" class="col-sm-4 col-form-label">Slider Button Text</label>
                                            <div class="col-sm-8">
                                              <div class="md-form mt-0">
                                                <input type="text" class="form-control" name="button_text" id="button_text" value="{{ $slider->button_text }}">
                                              </div>
                                            </div>
                                          </div>
                                          <!-- Grid row -->

                                          <!-- Grid row -->
                                          <div class="form-group row">
                                            <!-- Material input -->
                                            <label for="button_link" class="col-sm-4 col-form-label">Slider Button Link</label>
                                            <div class="col-sm-8">
                                              <div class="md-form mt-0">
                                                <input type="text" class="form-control" name="button_link" id="button_link" value="{{ $slider->button_link }}">
                                              </div>
                                            </div>
                                          </div>
                                          <!-- Grid row -->

                                          <!-- Grid row -->
                                          <div class="form-group row">
                                            <!-- Material input -->
                                            <label for="priority" class="col-sm-4 col-form-label">Priority</label>
                                            <div class="col-sm-8">
                                              <div class="md-form mt-0">
                                                <input type="text" class="form-control" name="priority" id="priority" value="{{ $slider->priority }}">
                                              </div>
                                            </div>
                                          </div>
                                          <!-- Grid row -->

                                          <!-- Grid row -->
                                          <div class="form-group row">
                                            <label class="col-sm-4 col-form-label"></label>
                                            <div class="col-sm-8">
                                              <button type="submit" class="btn btn-primary btn-md">Update Slider</button>
                                            </div>
                                          </div>
                                          <!-- Grid row -->
                                        </form>
                                        <!-- Horizontal material form -->
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--Delete Modal -->
                        <div class="modal fade text-center" id="deleteSlider{{ $slider->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
                             aria-hidden="true">

                            <!-- Add .modal-dialog-centered to .modal-dialog to vertically center the modal -->
                            <div class="modal-dialog modal-dialog-centered" role="document">

                                <div class="modal-content">
                                    <div class="modal-header text-center">
                                        <h4 class="modal-title text-danger" id="exampleModalLabel" >Delete!</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <h4 class=" text-danger" >Are You Sure To Delete!!!</h4>
                                    </div>
                                    <div class="modal-footer">
                                        <a href="{{ route('admin.slider.delete',$slider->id) }}" class="btn btn-danger" >Yes</a>
                                        <button  type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection