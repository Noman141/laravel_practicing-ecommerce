@extends('backend.layouts.master')

@section('content')

<div class="table-responsive text-nowrap">
    <h2 class="mt-3">Manage divisions</h2>
    <hr>
    @include('backend.partials.errormessage')
    <table class="table">
        <thead>
        <tr>
            <th scope="col">No.</th>
            <th scope="col">Name</th>
            <th scope="col">Priority</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>

        @php $i = 0; @endphp
        @foreach($divisions as $division)
        @php $i++; @endphp
        <tr>
            <th scope="row">{{ $i}}</th>
            <td>{{ $division->name }}</td>
            <td>{{ $division->priority }}</td>
            <td>
                <a class="btn btn-success btn-small" href="{{ route('admin.division.edit',$division->id) }}">Edit</a>
                <a class="btn btn-danger btn-small" href="#deletedivision{{ $division->id }}" data-toggle="modal" data-target="">Delete</a>

                <!--Delete Modal -->
                <div class="modal fade text-center" id="deletedivision{{ $division->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
                     aria-hidden="true">

                    <!-- Add .modal-dialog-centered to .modal-dialog to vertically center the modal -->
                    <div class="modal-dialog modal-dialog-centered" role="document">


                        <div class="modal-content">
                            <div class="modal-header text-center">
                                <h4 class="modal-title text-danger" id="exampleModalLabel" >Delete!</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <h4 class=" text-danger" >Are You Sure To Delete!!!</h4>
                            </div>
                            <div class="modal-footer">
                                <a href="{{ route('admin.division.delete',$division->id) }}" class="btn btn-danger" >Yes</a>
                                <button  type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>

@endsection