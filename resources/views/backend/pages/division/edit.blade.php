@extends('backend.layouts.master')

@section('content')
    <div class="container" style="width: 700px; margin: 0 auto;font-size: 30px !important;">
        <!-- Horizontal material form -->
        <h2 class="mt-3">Update Division</h2>
        <hr>
        {{--Validation message--}}
        @include('backend.partials.errormessage')
        <form action="{{ route('admin.division.update',$division->id) }}" method="post" enctype="multipart/form-data">
        @csrf
        <!-- Grid row -->
            <div class="form-group row">
                <!-- Material input -->
                <label for="name" class="col-sm-3 col-form-label">Division Name</label>
                <div class="col-sm-9">
                    <div class="md-form mt-0">
                        <input type="text" name="name" class="form-control" id="name" value="{{ $division->name }}">
                    </div>
                </div>
            </div>
            <!-- Grid row -->
            <!-- Grid row -->
            <div class="form-group row">
                <!-- Material input -->
                <label for="priority" class="col-sm-3 col-form-label">Priority</label>
                <div class="col-sm-9">
                    <div class="md-form mt-0">
                        <input type="number" name="priority" class="form-control" id="priority" value="{{ $division->priority }}" >
                    </div>
                </div>
            </div>
            <!-- Grid row -->

            <!-- Grid row -->
            <div class="form-group row">
                <label for="" class="col-sm-3 col-form-label"></label>
                <div class="col-sm-9">
                    <button type="submit" class="btn btn-primary btn-md">Update Division</button>
                </div>
            </div>
            <!-- Grid row -->
        </form>
        <!-- Horizontal material form -->
    </div>
@endsection