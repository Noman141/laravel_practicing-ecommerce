@extends('backend.layouts.master')

@section('content')

    <div class="container" style="width: 700px; margin: 0 auto;font-size: 30px !important;">
        <!-- Horizontal material form -->
        <h2 class="mt-3">Edit Products</h2>
        <hr>
        {{--Validation message--}}
        @include('backend.partials.errormessage')
        <form action="{{ route('admin.product.update',$product->id) }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <!-- Grid row -->
            <div class="form-group row">
                <!-- Material input -->
                <label for="title" class="col-sm-2 col-form-label">Title</label>
                <div class="col-sm-10">
                    <div class="md-form mt-0">
                        <input type="text" name="title" class="form-control" id="title" value="{{ $product->title }}">
                    </div>
                </div>
            </div>
            <!-- Grid row -->

            <!-- Grid row -->
            <div class="form-group row">
                <!-- Material input -->
                <label for="description" class="col-sm-2 col-form-label">Descriptions</label>
                <div class="col-sm-10">
                    <div class="md-form mt-0">
                        <textarea name="description" class="form-control" id="description"  rows="5">
                            {{ $product->description }}
                        </textarea>
                    </div>
                </div>
            </div>
            <!-- Grid row -->

            <!-- Grid row -->
            <div class="form-group row">
                <!-- Material input -->
                <label for="price" class="col-sm-2 col-form-label">Price</label>
                <div class="col-sm-10">
                    <div class="md-form mt-0">
                        <input type="number" name="price" class="form-control" id="price" value="{{ $product->price }}">
                    </div>
                </div>
            </div>
            <!-- Grid row -->

            <!-- Grid row -->
            <div class="form-group row">
                <!-- Material input -->
                <label for="quantity" class="col-sm-2 col-form-label">Quantity</label>
                <div class="col-sm-10">
                    <div class="md-form mt-0">
                        <input type="number" name="quantity" class="form-control" id="quantity" value="{{ $product->quantity }}">
                    </div>
                </div>
            </div>
            <!-- Grid row -->

            <!-- Grid row -->
            <div class="form-group row">
                <!-- Material input -->
                <label for="brand" class="col-sm-3 col-form-label">Choose Category</label>
                <div class="col-sm-9">
                    <div class="md-form mt-0">
                        <select class="mdb-select form-control" name="category_id">
                            <option value="" disabled selected>Choose One---</option>
                            @foreach(App\Models\Category::orderBy('name','asc')->where('parent_id',NULL)->get() as $parent)
                                <option value="{{ $parent->id }}">{{ $parent->name }}</option>
                                @foreach(App\Models\Category::orderBy('name','asc')->where('parent_id',$parent->id)->get() as $child)
                                    <option {!! $child->id ==  $product->category_id ? 'selected':'' !!} value="{{ $child->id }}">------>{{ $child->name }}</option>
                                @endforeach
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <!-- Grid row -->

            <!-- Grid row -->
            <div class="form-group row">
                <!-- Material input -->
                <label for="brand" class="col-sm-3 col-form-label">Choose Brand</label>
                <div class="col-sm-9">
                    <div class="md-form mt-0">
                        <select class="mdb-select form-control" name="brand_id">
                            <option value="" disabled selected>Choose One---</option>
                            @foreach(App\Models\Brand::orderBy('name','asc')->get() as $brand)
                                <option {!! $brand->id ==  $product->brand_id ? 'selected':'' !!} value="{{ $brand->id }}">{{ $brand->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <!-- Grid row -->

            <!-- Grid row -->
            <div class="form-group row">
                <!-- Material input -->
                <label for="image" class="col-sm-2 col-form-label">Upload Image</label>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="md-form mt-0">
                                <input type="file" name="image[]" class="form-control" id="image" placeholder="Choice File">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="md-form mt-0">
                                <input type="file" name="image[]" class="form-control" id="image" placeholder="Choice File">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="md-form mt-0">
                                <input type="file" name="image[]" class="form-control" id="image" placeholder="Choice File">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="md-form mt-0">
                                <input type="file" name="image[]" class="form-control" id="image" placeholder="Choice File">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Grid row -->

            <!-- Grid row -->
            <div class="form-group row">
                <label for="" class="col-sm-2 col-form-label"></label>
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary btn-md">Update Product</button>
                </div>
            </div>
            <!-- Grid row -->
        </form>
        <!-- Horizontal material form -->
    </div>

@endsection