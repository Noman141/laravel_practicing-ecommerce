@extends('backend.layouts.master')

{{--for page content--}}
@section('content')

   <div class="container">
       <h2 class="text-center my-3">Oder Details Of <b>NS{{ $order->id }}</b></h2>
       <hr>
       <h4>Order's Information</h4>
       <div class="row">
           <div class="col-md-6 border-right">
               <p><strong>Orderer Name: {{ $order->name }}</strong></p>
               <p><strong>Orderer Email: {{ $order->email }}</strong></p>
               <p><strong>Orderer Phone: {{ $order->phone_number }}</strong></p>
               <p><strong>Orderer Shipping Address: {{ $order->shipping_address }}</strong></p>
               <p><strong>Orderer Message: {{ $order->message }}</strong></p>
           </div>

           <div class="col-md-6 border-left">
               <p><strong>Orderer Payment Method: {{ $order->payment->name }}</strong></p>
               <p><strong>Orderer 	Transaction ID: {{ $order->transaction_id }}</strong></p>
           </div>
       </div>
       <hr>
       <h4>Order Items</h4>
       @if($order->cartItems->count() > 0)
           <h1>My Cart Items</h1>
           @include('backend.partials.errormessage')
       <table class="table table-striped">
           <thead class="black white-text">
           <tr>
               <th width="5%">No.</th>
               <th width="15%"> Name</th>
               <th width="20%"> Image</th>
               <th width="30%"> Quantity</th>
               <th width="10%">Unit Price</th>
               <th width="10%">Total Price</th>
               <th width="10%">Action</th>
           </tr>
           </thead>
           <tbody>
           @php
               $total_price = 0;
           @endphp
           @foreach($order->cartItems as $cart)
               <tr>
                   <th >{{ $loop->index +1 }}</th>
                   <td><a href="{{ route('product.show',$cart->product->slug) }}">{{ $cart->product->title }}</a></td>
                   <td>
                       @if($cart->product->images->count() > 0)
                           <img src="{{ asset('img/products/'.$cart->product->images->first()->image) }}" alt="{{ $cart->product->title }}" width="60px">
                       @endif
                   </td>
                   <td>
                       <form action="{{ route('carts.update',$cart->id) }}" class="form-inline" method="post">
                           @csrf
                           <input type="number" name="quantity" value="{{ $cart->quantity }}">
                           <button type="submit" class="btn btn-info btn-sm">Update</button>
                       </form>
                   </td>
                   <td>{{ $cart->product->price }}$</td>
                   <td>
                       @php
                           $total_price += $cart->product->price * $cart->quantity;
                       @endphp
                       {{ $cart->product->price * $cart->quantity}}$
                   </td>
                   <th scope="col">
                       <form action="{{ route('carts.delete',$cart->id) }}" class="form-inline" method="post">
                           @csrf
                           <input type="hidden" name="cart_id">
                           <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                       </form>
                   </th>
               </tr>
           @endforeach
           <tr class="text-danger">
               <td colspan="4"></td>
               <td><b>Total Amount</b></td>
               <td><b>{{$total_price}}$</b></td>
               <td></td>
           </tr>
           </tbody>
       </table>
       @endif

       <hr>
       <div class="row">
           <div class="col-md-7"></div>
           <form action="{{ route('admin.order.completed',$order->id) }}" method="post" class="form-inline">
               @csrf
               @if($order->is_competed)
                   <input type="submit" value="Cancel Order" class="btn btn-danger">
                   @else
                   <input type="submit" value="Completed" class="btn btn-success">
               @endif
           </form>
           <form action="{{ route('admin.order.paid',$order->id) }}" method="post" class="form-inline">
               @csrf
               @if($order->is_paid)
                   <input type="submit" value="Cancel Payment" class="btn btn-danger">
                   @else
                   <input type="submit" value="Paid" class="btn btn-success">
               @endif
           </form>
       </div>
   </div>

@endsection

{{--for page js--}}
@section('scripts')
    <script></script>
@endsection