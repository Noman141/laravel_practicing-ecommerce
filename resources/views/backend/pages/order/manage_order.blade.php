@extends('backend.layouts.master')

@section('content')
    <div class="table-responsive text-nowrap">
        <h2 class="mt-3">Manage Orders</h2>
        <hr>
        @include('backend.partials.errormessage')
        <table class="table" id="myTable">
            <thead>
            <tr>
                <th scope="col">No.</th>
                <th scope="col">Order ID</th>
                <th scope="col">Orderer Name</th>
                <th scope="col">Orderer Phone No.</th>
                <th scope="col">Order Status</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>

            @php $i = 0; @endphp
            @foreach($orders as $order)
                @php $i++; @endphp
                <tr>
                    <th scope="row">{{ $i}}</th>
                    <td>#NS-{{ $order->id }}</td>
                    <td>{{ $order->name }}</td>
                    <td>{{ $order->phone_number }}</td>

                    <td>
                        <p>
                            @if($order->is_seen_by_admin)
                                <button type="button" class="btn btn-success btn-sm">Seen</button>
                            @else
                                <button type="button" class="btn btn-warning btn-sm">Unseen</button>
                            @endif
                        </p>

                        <p>
                            @if($order->is_competed)
                                <button type="button" class="btn btn-success btn-sm">Completed</button>
                            @else
                                <button type="button" class="btn btn-warning btn-sm"> Not completed</button>
                            @endif
                        </p>

                        <p>
                            @if($order->is_paid)
                                <button type="button" class="btn btn-success btn-sm">Paid</button>
                            @else
                                <button type="button" class="btn btn-danger btn-sm">Unpaid</button>
                            @endif
                        </p>
                    </td>

                    <td>
                        <a class="btn btn-info btn-small" href="{{ route('admin.order.show',$order->id) }}" >View Order</a>
                        <a class="btn btn-danger btn-small" href="#deleteProduct{{ $order->id }}" data-toggle="modal" data-target="">Delete</a>

                        <!--Delete Modal -->
                        <div class="modal fade text-center" id="deleteProduct{{ $order->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
                             aria-hidden="true">

                            <!-- Add .modal-dialog-centered to .modal-dialog to vertically center the modal -->
                            <div class="modal-dialog modal-dialog-centered" role="document">


                                <div class="modal-content">
                                    <div class="modal-header text-center">
                                        <h4 class="modal-title text-danger" id="exampleModalLabel" >Delete!</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <h4 class=" text-danger" >Are You Sure To Delete!!!</h4>
                                    </div>
                                    <div class="modal-footer">
                                        <a href="{{ route('admin.order.delete',$order->id) }}" class="btn btn-danger" >Yes</a>
                                        <button  type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection