@extends('backend.layouts.master')

@section('content')
    <div class="container" style="width: 700px; margin: 0 auto;font-size: 30px !important;">
        <!-- Horizontal material form -->
        <h2 class="mt-3">Edit District</h2>
        <hr>
        {{--Validation message--}}
        @include('backend.partials.errormessage')
        <form action="{{ route('admin.district.update',$districts->id)}}" method="post" enctype="multipart/form-data">
        @csrf
        <!-- Grid row -->
            <div class="form-group row">
                <!-- Material input -->
                <label for="name" class="col-sm-3 col-form-label">District Name</label>
                <div class="col-sm-9">
                    <div class="md-form mt-0">
                        <input type="text" name="name" class="form-control" id="name" value="{{ $districts->name }}">
                    </div>
                </div>
            </div>
            <!-- Grid row -->
            <!-- Grid row -->
            <div class="form-group row">
                <!-- Material input -->
                <label for="division_id" class="col-sm-3 col-form-label">Division Name</label>
                <div class="col-sm-9">
                    <div class="md-form mt-0">
                        <select class=" browser-default custom-select form-control" name="division_id" id="my-Select">
                            <option value="" disabled selected>Choose One---</option>
                            @foreach($divisions as $division)
                                <option value="{{ $division->id }}"{{ $districts->division->id == $division->id ? 'selected':''}} >{{ $division->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <!-- Grid row -->

            <!-- Grid row -->
            <div class="form-group row">
                <label for="" class="col-sm-3 col-form-label"></label>
                <div class="col-sm-9">
                    <button type="submit" class="btn btn-primary btn-md">Update District</button>
                </div>
            </div>
            <!-- Grid row -->
        </form>
        <!-- Horizontal material form -->
    </div>
@endsection

@section('scripts')
    <script>
        // Material Select Initialization
        // $('.mdb-select').materialSelect('destroy');
    </script>
@endsection