<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>NeerShop Admin Dashboard Template</title>
{{----}}

<!-- Fonts -->
    {{--<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">--}}
    {{--<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/mdb.min.css') }}">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <!-- plugins:css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/admin/materialdesignicons.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/admin/simple-line-icons.css') }}">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->

    <link rel="stylesheet" href="{{ asset('css/admin/datatables.min.css') }}">
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('css/admin/style.css') }}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}" />
</head>

<body>
<div class="container-scroller">
    @include('backend.partials.nav')
    <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
         @include('backend.partials.sidebar')

        <div class="main-panel px-5">
            @yield('content')
            <!-- partial:partials/_footer.html -->
            <footer class="footer">
                <div class="container-fluid clearfix">
                    <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 <a href="#" target="_blank">NeerShop</a>. All rights reserved.</span>
                    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Design @ Developed By - Noman <i class="fas fa-heart text-danger"></i></span>
                </div>
            </footer>
            <!-- partial -->
        </div>
        <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->

<!-- plugins:js -->
<script type="text/javascript" src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/popper.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/mdb.min.js') }}"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<script src="{{ asset('backend') }}"></script>
<!-- End plugin js for this page-->
<!-- inject:js -->
<script type="text/javascript" src="{{ asset('js/admin/off-canvas.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/admin/misc.js') }}"></script>
<!-- endinject -->

<script type="text/javascript" src="{{ asset('js/admin/datatables.min.js') }}"></script>
<!-- Custom js for this page-->
<script type="text/javascript" src="{{ asset('js/admin/dashboard.js') }}"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB5NXz9eVnyJOA81wimI8WYE08kW_JMe8g&callback=initMap" async defer></script>
<script type="text/javascript" src="{{ asset('js/admin/maps.js') }}"></script>
<!-- End custom js for this page-->

<script>
    function myFunction() {
        var x = document.getElementById("myDIV");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }
</script>

<script>
    function myFunction2() {
        var y = document.getElementById("2ndDropDown");
        if (y.style.display === "none") {
            y.style.display = "block";
        } else {
            y.style.display = "none";
        }
    }
</script>

<script>
    $(document).ready( function () {
        $('#myTable').DataTable();
    } );
</script>
<script>
    // Material Select Destroy

</script>

@yield('scripts')
</body>

</html>