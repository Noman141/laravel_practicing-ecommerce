<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item nav-profile">
            <div class="nav-link">
                <div class="profile-image"> <img src="{{ asset('images/faces/face4.jpg') }}" alt="image"/> <span class="online-status online"></span> </div>
                <div class="profile-name">
                    <p class="name">Richard V.Welsh</p>
                    <p class="designation">Manager</p>
                    <div class="badge badge-teal mx-auto mt-3">Online</div>
                </div>
            </div>
        </li>
        <li class="nav-item"><a class="nav-link" href="index.html"><img class="menu-icon" src="{{ asset('images/menu_icons/01.png') }}" alt="menu icon"><span class="menu-title">Dashboard</span></a></li>
        <li class="nav-item"><a class="nav-link" href="pages/widgets.html"><img class="menu-icon" src="{{ asset('images/menu_icons/02.png') }}" alt="menu icon"><span class="menu-title">Widgets</span></a></li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#manage-order" aria-expanded="false" aria-controls="general-pages"> <img class="menu-icon" src="{{ asset('images/menu_icons/08.png') }}" alt="menu icon"> <span class="menu-title">Manage Orders </span> <i class="fas fa-bars ml-5"></i></a>
            <div class="collapse" id="manage-order">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"><a class="nav-link" href="{{ route('admin.manage_order') }}"> <span class="menu-title">Orders List</span></a></li>
                    {{--<li class="nav-item"><a class="nav-link" href="{{ route('admin.order.create') }}"> <span class="menu-title">Create District</span></a></li>--}}
                </ul>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#manage-product" aria-expanded="false" aria-controls="general-pages"> <img class="menu-icon" src="{{ asset('images/menu_icons/08.png') }}" alt="menu icon"> <span class="menu-title">Manage Products </span> <i class="fas fa-bars ml-5"></i></a>
            <div class="collapse" id="manage-product">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"><a class="nav-link" href="{{ route('admin.manage_product') }}"> <span class="menu-title">Products List</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('admin.product.create') }}"> <span class="menu-title">Create Products</span></a></li>
                </ul>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#manage-category" aria-expanded="false" aria-controls="general-pages"> <img class="menu-icon" src="{{ asset('images/menu_icons/08.png') }}" alt="menu icon"> <span class="menu-title">Manage Category </span> <i class="fas fa-bars ml-5"></i></a>
            <div class="collapse" id="manage-category">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"><a class="nav-link" href="{{ route('admin.manage_category') }}"> <span class="menu-title">Category List</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('admin.category.create') }}"> <span class="menu-title">Create Category</span></a></li>
                </ul>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#manage-brand" aria-expanded="false" aria-controls="general-pages"> <img class="menu-icon" src="{{ asset('images/menu_icons/08.png') }}" alt="menu icon"> <span class="menu-title">Manage Brand </span> <i class="fas fa-bars ml-5"></i></a>
            <div class="collapse" id="manage-brand">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"><a class="nav-link" href="{{ route('admin.manage_brand') }}"> <span class="menu-title">Brand List</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('admin.brand.create') }}"> <span class="menu-title">Create Brand</span></a></li>
                </ul>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#manage-division" aria-expanded="false" aria-controls="general-pages"> <img class="menu-icon" src="{{ asset('images/menu_icons/08.png') }}" alt="menu icon"> <span class="menu-title">Manage Division </span> <i class="fas fa-bars ml-5"></i></a>
            <div class="collapse" id="manage-division">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"><a class="nav-link" href="{{ route('admin.manage_division') }}"> <span class="menu-title">Division List</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('admin.division.create') }}"> <span class="menu-title">Create Division</span></a></li>
                </ul>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#manage-district" aria-expanded="false" aria-controls="general-pages"> <img class="menu-icon" src="{{ asset('images/menu_icons/08.png') }}" alt="menu icon"> <span class="menu-title">Manage District </span> <i class="fas fa-bars ml-5"></i></a>
            <div class="collapse" id="manage-district">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"><a class="nav-link" href="{{ route('admin.manage_district') }}"> <span class="menu-title">District List</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('admin.district.create') }}"> <span class="menu-title">Create District</span></a></li>
                </ul>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#manage-slider" aria-expanded="false" aria-controls="general-pages"> <img class="menu-icon" src="{{ asset('images/menu_icons/08.png') }}" alt="menu icon"> <span class="menu-title">Manage Sliders </span> <i class="fas fa-bars ml-5"></i></a>
            <div class="collapse" id="manage-slider">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"><a class="nav-link" href="{{ route('admin.manage_slider') }}"> <span class="menu-title">Slider List</span></a></li>
                </ul>
            </div>
        </li>

        <li class="nav-item">
            <a href="#" class="nav-link">
                <form action="{{ asset(route('admin.logout')) }}" class="form-inline" method="post">
                    @csrf
                    <input type="submit" value="Log Out" class="btn btn-danger btn-sm">
                </form>
            </a>
        </li>

    </ul>
</nav>
<!-- partial -->